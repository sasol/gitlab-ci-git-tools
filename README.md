# GitLab Ci Git Tools

Set of bash scripts for pushing back to repo during CI builds in Gitlab CI.

Probably it is in general a bad practice, but I wanted to keep terraform config in git
together with terraform state and manage terraform scripts via CI pipelines. 

## Usage

Set GLCIT_SSH_PRIVATE_KEY secret to contain priavte key that can be used to access repo. This can
be a users key or projects 'deploy' key (Settings/Repository/Deploy Keys).

Then create jobs that use docker image (eg `registry.gitlab.com/sasol/gitlab-ci-git-tools:latest`) containing helper scripts or install them to your script:
```
curl -s https://gitlab.com/sasol/gitlab-ci-git-tools/raw/$GLCIT_VERSION/get-gitlab-ci-git-tools.sh | bash /dev/stdin $GLCIT_VERSION
```
where $GLCIT_VERSION is a tag from this project repo.

See `.gitlab-ci.yml` in this project sources for examples.

## Commands
* `git-edit` - converts detached head to ci_processing local branch and replaces remote to be 
    in 'ssh' format without token.
* `git-commit [message]` - adds all changed and un tracked files and commits with message containing
    build details
* `git-push [remote] [branch]` - pushes forcibly to remote and branch. To disable force set
    DISABLE_FORCE_PUSH variable. remote default is origin, branch default is current build branch.
* `git-push-url <url> [branch]` - pushes forcibly to url and branch. To disable force set 
    DISABLE_FORCE_PUSH variable. branch default is current build branch.  
* `git-pull [remote] [branch]` - reverts all local changes and pulls from project repo. 
    remote default is origin, branch default is current build branch.
* `source ssh-set-key` - extracts ssh key from GLCIT_SSH_PRIVATE_KEY variable and adds it to ssh-agent.
    used by other scripts.
* `ssh-know-host <url>` - registers host in ssh known_hosts file.
