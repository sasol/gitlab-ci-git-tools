#!/usr/bin/env bash

if [ -z "$1" ]; then
        >&2 echo "No version parameter found"
        exit 1
fi

GLCIT_VERSION=$1

curl https://gitlab.com/sasol/gitlab-ci-git-tools/raw/$GLCIT_VERSION/scripts/git-push-url -o /usr/local/bin/git-push-url
chmod +x /usr/local/bin/git-push-url

curl https://gitlab.com/sasol/gitlab-ci-git-tools/raw/$GLCIT_VERSION/scripts/git-push -o /usr/local/bin/git-push
chmod +x /usr/local/bin/git-push

curl https://gitlab.com/sasol/gitlab-ci-git-tools/raw/$GLCIT_VERSION/scripts/git-commit -o /usr/local/bin/git-commit
chmod +x /usr/local/bin/git-commit

curl https://gitlab.com/sasol/gitlab-ci-git-tools/raw/$GLCIT_VERSION/scripts/git-edit -o /usr/local/bin/git-edit
chmod +x /usr/local/bin/git-edit

curl https://gitlab.com/sasol/gitlab-ci-git-tools/raw/$GLCIT_VERSION/scripts/git-pull -o /usr/local/bin/git-pull
chmod +x /usr/local/bin/git-pull

curl https://gitlab.com/sasol/gitlab-ci-git-tools/raw/$GLCIT_VERSION/scripts/ssh-set-key -o /usr/local/bin/ssh-set-key
chmod +x /usr/local/bin/ssh-set-key

curl https://gitlab.com/sasol/gitlab-ci-git-tools/raw/$GLCIT_VERSION/scripts/ssh-know-host -o /usr/local/bin/ssh-know-host
chmod +x /usr/local/bin/ssh-know-host

