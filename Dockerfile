FROM alpine:3.7

RUN apk add --update --no-cache bash sed git openssh-client && rm -rf /var/cache/apk/*

ADD scripts/git-push-url /usr/local/bin/git-push-url
RUN chmod +x /usr/local/bin/git-push-url

ADD scripts/git-push /usr/local/bin/git-push
RUN chmod +x /usr/local/bin/git-push

ADD scripts/git-commit /usr/local/bin/git-commit
RUN chmod +x /usr/local/bin/git-commit

ADD scripts/git-edit /usr/local/bin/git-edit
RUN chmod +x /usr/local/bin/git-edit

ADD scripts/git-pull /usr/local/bin/git-pull
RUN chmod +x /usr/local/bin/git-pull

ADD scripts/ssh-set-key /usr/local/bin/ssh-set-key
RUN chmod +x /usr/local/bin/ssh-set-key

ADD scripts/ssh-know-host /usr/local/bin/ssh-know-host
RUN chmod +x /usr/local/bin/ssh-know-host

